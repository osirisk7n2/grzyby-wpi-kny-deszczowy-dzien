# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
gniazdo = socket.socket(socket.AF_INET,
                        socket.SOCK_STREAM,
                        socket.IPPROTO_IP)
# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31012)  # TODO: zmienić port!
gniazdo.bind(server_address)
# Nasłuchiwanie przychodzących połączeń
gniazdo.listen(4);
while True:
    # Czekanie na połączenie
    connection, client_address = gniazdo.accept()
    try:
        # Odebranie danych i odesłanie ich spowrotem
        zmienna = connection.recv(1024)
        connection.sendall(str(zmienna))
        pass

    finally:
        # Zamknięcie połączenia
        connection.close()
        pass
