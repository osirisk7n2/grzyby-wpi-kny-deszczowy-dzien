# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Połączenie z gniazdem nasłuchującego serwera
server_address = ('194.29.175.240', 31012)  # TODO: zmienić port!
client_socket.connect(server_address)

try:
    # Wysłanie danych
    message = u'To jest wiadomosc, ktora zostanie zwrocona.'
    client_socket.send(message)

    # Wypisanie odpowiedzi
    data = client_socket.recv(512)
    print(data)
finally:
    # Zamknięcie połączenia
    client_socket.close()
    pass